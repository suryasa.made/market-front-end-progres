<nav class="navbar navbar-default megamenu">
    <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#defaultmenu" class="navbar-toggle">
        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
        </button>
    </div>
    <!-- end navbar-header -->
    <div id="defaultmenu" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <!-- standard drop down -->
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"> Master Data <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                        <a href="#">Data Item </a>
                        <ul class="dropdown-menu">
                            <li><a href="daftar_item">Daftar Item</a></li>
                            <li><a href="tambah_item">Tambah Item </a></li>
                            <li><a href="kartu_stok">Kartu Stok </a></li>
                            <li><a href="datasheet">Datasheet </a></li>
                            <li><a href="diskon_periode">Diskon Periode </a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#">Data-Data </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Daftar Supplier</a></li>
                            <li><a href="">Daftar Pelanggan </a></li>
                            <li><a href="">Group Pelanggan </a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#">Data Pendukung </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Satuan</a></li>
                            <li><a href="">Jenis </a></li>
                            <li><a href="">Bank </a></li>
                            <li><a href="">Merek </a></li>
                            <li><a href="">Dept./Gudang </a></li>
                        </ul>
                    </li>
                </ul>
                <!-- end dropdown-menu -->
            </li>
            <!-- end standard drop down -->
            <!-- standard drop down -->
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"> Pembelian <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                        <a href="#">Pesanan </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Pesanan Pembelian</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#">Pembelian </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Daftar Pembelian</a></li>
                            <li><a href="">Hisory Harga Beli </a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#">Retur </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Retur</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- end dropdown-menu -->
            </li>
            <!-- end standard drop down -->
            <!-- standard drop down -->
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"> Penjualan <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                        <a href="#">Pesanan </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Pesanan Penjualan</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#">Penjualan </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Daftar Penjualan</a></li>
                            <li><a href="">Daftar Penjualan Kasir</a></li>
                            <li><a href="">Hisory Harga Jual </a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#">Retur </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Retur Penjualan</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- end dropdown-menu -->
            </li>
            <!-- end standard drop down -->
            <!-- standard drop down -->
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"> Persediaan <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                        <a href="#">Penyesuaian </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Daftar Item Masuk</a></li>
                            <li><a href="">Daftar Item Keluar</a></li>
                            <li><a href="">Stok Opname</a></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <a href="#">Transfer </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Transfer Item</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- end dropdown-menu -->
            </li>
                <!-- standard drop down -->
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"> Laporan <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                        <a href="#">Pembelian </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Laporan Pembelian</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- end dropdown-menu -->
            </li>
            <!-- end standard drop down -->
            <!-- standard drop down -->
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle"> Pengaturan <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                        <a href="#">Sistem </a>
                        <ul class="dropdown-menu">
                            <li><a href="">Data User</a></li>
                            <li><a href="">Data Perusahaan</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- end dropdown-menu -->
            </li>
            <!-- end standard drop down -->
        </ul>
        <!-- end nav navbar-nav -->
    </div>
    <!-- end #navbar-collapse-1 -->
</nav>

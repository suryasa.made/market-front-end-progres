<!DOCTYPE html>
<html lang="en">
@include('admin/template/head')
    <body>
        <div id="container" class="effect mainnav-full">
            <!--NAVBAR-->
            @include('admin/template/header')
          <div class="boxed">
              <!--CONTENT CONTAINER-->
              <!--===================================================-->
              <div id="content-container">
                  <div class="pageheader">
                      <h3><i class="fa fa-home"></i> @yield('title') </h3>
                      <div class="breadcrumb-wrapper">
                          <ol class="breadcrumb">
                              <li> <a href="#"> @yield('maintitle') </a> </li>
                              <li class="active"> @yield('title') </li>
                          </ol>
                      </div>
                  </div>
                    <!--Page content-->
                    <!--===================================================-->
                    @yield('main')
                    <!--===================================================-->
                    <!--End page content-->
                </div>
                <!--===================================================-->
                <!--END CONTENT CONTAINER-->
            </div>
            <!-- FOOTER -->
            <!--===================================================-->
            @include('admin/template/footer')
            <!--===================================================-->
            <!-- END FOOTER -->
            <!-- SCROLL TOP BUTTON -->
            <!--===================================================-->
            <button id="scroll-top" class="btn"><i class="fa fa-chevron-up"></i></button>
            <!--===================================================-->
        </div>
        <!--===================================================-->
        <!-- END OF CONTAINER -->
        <!--JAVASCRIPT-->
        <!--=================================================-->
        @yield('script')
    </body>
</html>

@extends('admin/template/template')
@section('maintitle', 'Data Item')
@section('title', 'Tambah Item Baru')
@section('main')
<div id="page-content">

  <form  id="formtambahitem" class="form-horizontal">
  <div class="row">
      <div class="eq-height">
          <div class="col-lg-6">
              <div class= "panel">
                  <div class="panel-heading">
                      <h3 class="panel-title">Data Umum</h3>
                  </div>
                  <!--Block Styled Form -->
                  <!--===================================================-->
                  {{-- <form class="form-horizontal"> --}}
                      <div class="panel-body">
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Tipe Item</label>
                            <div class="col-xs-8">
                              <div class="col-md-6 pad-no">
                                  <!-- Icon Radio Buttons -->
                                  <div class="radio">
                                      <label class="form-radio form-icon">
                                      <input type="radio" name="tipe-item" value="barang"> Barang (INV)</label>
                                  </div>
                                  <div class="radio">
                                      <label class="form-radio form-icon">
                                      <input type="radio" name="tipe-item" value="rakitan"> Rakitan (ASM)</label>
                                  </div>
                                  <div class="radio">
                                      <label class="form-radio form-icon">
                                      <input type="radio" name="tipe-item" value="non-inventory"> Non Inventory (NON-INV)</label>
                                  </div>
                                  <div class="radio">
                                      <label class="form-radio form-icon">
                                      <input type="radio" name="tipe-item" value="jasa"> Jasa (SRV)</label>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Kode Item*</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="kode-item" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Nama Item*</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="nama-item" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Jenis*</label>
                            <div class="col-xs-6">
                              <select class="form-control selectpicker" data-live-search="true">
                                  <option value="makanan">Makanan</option>
                                  <option value="minuman">Minuman</option>
                              </select>
                            </div>
                            <div class="col-xs-3">
                                <div class="btn btn-default btn-icon icon-lg fa fa-plus" data-toggle="modal" data-target="#tambahJenis"></div>
                            </div>
                            <div class="modal fade" id="tambahJenis" tabindex="-1" role="dialog" aria-labelledby="tambahJenisTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLongTitle">Jenis Item</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Jenis</th>
                                                    <th>Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" class="form-control" name="jenis" /></td>
                                                    <td><input type="text" class="form-control" name="keterangan" /></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="text" class="form-control" name="jenis" /></td>
                                                    <td><input type="text" class="form-control" name="keterangan" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                      <button type="button" class="btn btn-primary">Hapus</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Merek</label>
                            <div class="col-xs-6">
                              <select class="form-control selectpicker" data-live-search="true">
                                  <option value="indofood">Indofood</option>
                              </select>
                            </div>
                            <div class="col-xs-3">
                                <div class="btn btn-default btn-icon icon-lg fa fa-plus" data-toggle="modal" data-target="#tambahMerek"></div>
                            </div>
                            <div class="modal fade" id="tambahMerek" tabindex="-1" role="dialog" aria-labelledby="tambahMerekTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLongTitle">Jenis Item</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Merek</th>
                                                    <th>Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" class="form-control" name="merek" /></td>
                                                    <td><input type="text" class="form-control" name="keterangan" /></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="text" class="form-control" name="merek" /></td>
                                                    <td><input type="text" class="form-control" name="keterangan" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                      <button type="button" class="btn btn-primary">Hapus</button>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Mata Uang</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="mata-uang" value="IDR" readonly />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Rak</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="rak" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Dept. / Gudang</label>
                            <div class="col-xs-8">
                              <select class="form-control">
                                  <option value="gudang">Gudang</option>
                                  <option value="utama">Utama</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Keterangan</label>
                            <div class="col-xs-8">
                                <textarea name="keterangan" placeholder="Keterangan" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Status Jual</label>
                            <div class="col-xs-8">
                                <div class="radio">
                                    <label class="form-radio form-icon">
                                        <input type="radio" name="status-jual" value="masih-dijual"> Masih dijual
                                    </label>
                                    <label class="form-radio form-icon">
                                        <input type="radio" name="status-jual" value="tidak-dijual"> Tidak dijual
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-3 control-label">Pilihan</label>
                            <div class="col-xs-8">
                                <div class="checkbox">
                                    <input type="checkbox" name="pilihan" value="serial"> Serial
                                </div>
                                <div class="checkbox">
                                    <input type="checkbox" name="pilihan" value="serial"> Tidak dapat point
                                </div>
                            </div>
                        </div>
                      </div>
                  {{-- </form> --}}
                  <!--===================================================-->
                  <!--End Block Styled Form -->
              </div>

              <div class="panel">
                  <div class="panel-heading">
                      <h3 class="panel-title">Lain - lain</h3>
                  </div>
                  <!-- Form-->
                  <!--===================================================-->
                  {{-- <form class="form-horizontal"> --}}
                      <div class="panel-body">
                          <div class="form-group">
                            <label class="col-xs-3 control-label">Stok Minimum</label>
                            <div class="col-xs-8">
                              <input type="text" class="form-control" name="stok-minimum" />
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-xs-3 control-label">Gambar</label>
                            <div class="col-xs-8">
                              <img style="border:1px">
                              <input type="file" name="foto-stok-minimun" id="foto-stok-minimun">
                            </div>
                          </div>
                      </div>
                  {{-- </form> --}}
                  <!--===================================================-->
                  <!--End  Form-->
              </div>
          </div>
          <div class="col-lg-6">
              <div class="panel">
                  <div class="panel-heading">
                      <h3 class="panel-title">Harga Jual</h3>
                  </div>
                  <!--Horizontal Form-->
                  <!--===================================================-->
                  {{-- <form class="form-horizontal"> --}}
                      <div class="panel-body">
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Satuan*</label>
                              <div class="col-sm-8">
                                  <select class="form-control selectpicker">
                                      <option value="dus">DUS</option>
                                      <option value="pak">PAK</option>
                                      <option value="pcs">PCS</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-3 control-label">Pilihan Harga Jual*</label>
                              <div class="col-sm-8">
                                <select id="pilihanhargajual" class="form-control selectpicker">
                                    <option value="harga">Satu Harga</option>
                                    <option value="jumlah">Berdasarkan Jumlah</option>
                                </select>
                              </div>
                          </div>
                          <div id="p1" class="form-group pilihanhargajual">
                              <label class="col-xs-3 control-label">Harga jual*</label>
                              <div class="col-xs-8">
                                  <input type="text" class="form-control" name="harga-jual" />
                              </div>
                          </div>
                          {{-- <div id="p2" class="pilihanhargajual" hidden>
                            <div class="form-group">
                              <div class="col-xs-1">
                              </div>
                                <label class="col-xs-5 ">Jml Sampai*</label>
                                <label class="col-xs-5 ">Harga jual*</label>
                            </div>
                            <div class="form-group">
                              <div class="col-xs-1">
                              </div>
                              <div class="col-xs-5">
                                  <input type="text" class="form-control" name="username" />
                              </div>
                                <div class="col-xs-5">
                                    <input type="text" class="form-control" name="username" />
                                </div>
                            </div>
                          </div> --}}
                      </div>
                  {{-- </form> --}}
                  <!--===================================================-->
                  <!--End Horizontal Form-->
              </div>

              <div class="panel">
                  <div class="panel-heading">
                      <h3 class="panel-title">Potongan Harga</h3>
                  </div>
                  <!--Horizontal Form-->
                  <!--===================================================-->
                  {{-- <form class="form-horizontal"> --}}
                      <div class="panel-body">
                          <div class="form-group">
                            <div class="col-xs-1">
                            </div>
                              <label class="col-xs-5 ">Group Pelanggan</label>
                              <label class="col-xs-5 ">Potongan (%)</label>
                          </div>
                          <div class="form-group">
                            <div class="col-xs-1">
                            </div>
                            <div class="col-xs-5">
                              <select class="form-control selectpicker">
                                  <option value="dus">DUS</option>
                                  <option value="pak">PAK</option>
                                  <option value="pcs">PCS</option>
                              </select>
                            </div>
                              <div class="col-xs-5">
                                  <input type="text" class="form-control"/>
                              </div>
                          </div>
                      </div>
                  {{-- </form> --}}
                  <!--===================================================-->
                  <!--End Horizontal Form-->
              </div>
          </div>
      </div>
  </div>

  </form>
  <button class="btn btn-primary btn-labeled fa fa-save" name="simpan">Simpan</button>
  <button class="btn btn-default btn-labeled fa fa-mail-reply" name="batal">Batal</button>
</div>
@endsection


@section('script')
<!--jQuery [ REQUIRED ]-->
<script src="{{ asset('atem/js/jquery-2.1.1.min.js') }}"></script>
<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/bootstrap.min.js') }}"></script>
<!--Fast Click [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/fast-click/fastclick.min.js') }}"></script>
<!--Jquery Nano Scroller js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/nanoscrollerjs/jquery.nanoscroller.min.js') }}"></script>
<!--Metismenu js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/metismenu/metismenu.min.js') }}"></script>
<!--Jasmine Admin [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/scripts.js') }}"></script>
<!--Switchery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/switchery/switchery.min.js') }}"></script>
<!--Bootstrap Select [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<!--DataTables [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<!--Fullscreen jQuery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/screenfull/screenfull.js') }}"></script>
<!--DataTables Sample [ SAMPLE ]-->
<script src="{{ asset('atem/plugins/dropzone/dropzone.min.js') }}"></script>
<!--Dropzone [ OPTIONAL ]-->
<script>
  // Row selection and deletion (multiple rows)
  // -----------------------------------------------------------------
  var rowDeletion = $('#demo-dt-delete').DataTable({
    //"responsive": true,
    "scrollX": true,
    "searching": false,
    "paging": false,
    "info": false,
    "dom": 'frtip<"toolbar">',
    "aoColumnDefs": [
        { "bSortable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12 ] },
        { "bSearchable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12 ] }
    ]
  });

  $('#demo-custom-toolbar').appendTo($("div.toolbar "));

  $('#demo-dt-delete tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
      $(this).removeClass('selected');
    }
    else {
      rowDeletion.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      //$(this).toggleClass('selected');
    }
  } );

  $('#hapusitem').click( function () {
    rowDeletion.row('.selected').remove().draw( false );
  } );

  $(document).ready(function(){
      $("#pilihanhargajual").change(function(){
          $(this).find("option:selected").each(function(){
              var optionValue = $(this).attr("value");
              $(".pilihanhargajual").hide();
              $("#p" + optionValue).show();

              console.log(optionValue);
          });
      }).change();
  });
</script>
@endsection

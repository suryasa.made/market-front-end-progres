@extends('admin/template/template')
@section('maintitle', 'Data Item')
@section('title', 'Kartu Stok')
@section('main')
<div id="page-content">

    <!-- Row selection (single row) -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Kartu Stok</h3>
        </div>
        <form class="form-horizontal">
          <div class="panel-body">
              <div class="form-group">
                <label class="col-md-2 col-sm-2">Kode Item</label>
                <div class="col-md-5 col-sm-5">
                  <input type="text" class="form-control" name="kode-item" />
                </div>
                <div class="col-md-1 col-sm-1">
                  <div class="btn btn-primary btn-icon icon-lg fa fa-archive" id="daftar-item" data-toggle="modal" data-target="#daftarItem"></div>
                </div>
                <div class="modal fade" id="daftarItem" tabindex="-1" role="dialog" aria-labelledby="daftarItemTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Daftar Item</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        {{-- Start of modal body --}}
                        <div class="modal-body">
                            {{-- <div class="form-horizontal"> --}}
                                {{-- <div class="panel-body"> --}}
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Kata Kunci 1</label>
                                      <div class="col-md-4 col-sm-4">
                                        <input type="text" class="form-control" name="kata-kunci1" />
                                      </div>
                                      <label class="col-md-2 col-sm-2 control-label">Merek</label>
                                      <div class="col-md-4 col-sm-4">
                                        <select class="form-control" name="merek">
                                            <option value="merek1">Merek 1</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Kata Kunci 2</label>
                                      <div class="col-md-4 col-sm-4">
                                        <input type="text" class="form-control" name="kata-kunci2" />
                                      </div>
                                      <label class="col-md-3 col-sm-3"> Jenis / Satuan / Rak</label>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Kata Kunci 3</label>
                                      <div class="col-md-4 col-sm-4">
                                        <input type="text" class="form-control" name="kata-kunci3" />
                                      </div>
                                      <label class="col-md-2 col-sm-2"> Keterangan</label>
                                      <label class="col-md-2 col-sm-2 control-label">Kantor / Gudang</label>
                                      <div class="col-md-2 col-sm-2">
                                        <select class="form-control" name="lokasi">
                                            <option value="gudang">Gudang</option>
                                            <option value="utama">Utama</option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 control-label">Kode Supplier</label>
                                        <div class="col-md-4 col-sm-4">
                                          <input type="text" class="form-control" name="kode-supplier" />
                                        </div>
                                      </div>
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Urut berdasar</label>
                                      <div class="col-md-3 col-sm-3">
                                        <select class="form-control">
                                            <option value="kode-item">Kode Item</option>
                                            <option value="nama-item">Nama Item</option>
                                            <option value="jenis">Jenis</option>
                                            <option value="merek">Merek</option>
                                            <option value="keterangan">Keterangan</option>
                                            <option value="supplier">Supplier</option>
                                        </select>
                                      </div>
                                      <div class="col-md-2 col-sm-2">
                                          <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-asc" id="sort-asc"></div>
                                          <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-desc" id="sort-desc"></div>
                                      </div>
                                      <div class="col-md-1 col-sm-1"></div>
                                      <div class="col-md-2 col-sm-2">
                                          <div class="btn btn-default btn-icon icon-lg fa fa-search" id="search"></div>
                                          <div class="btn btn-default btn-icon icon-lg fa fa-remove" id="reset"></div>
                                      </div>
                                    </div>
                                  <table id="demo-dt-delete" class="table table-striped table-bordered" width="1800px">
                                      <thead>
                                          <tr>
                                            <th>No</th>
                                            <th>Kode Item</th>
                                            <th>Barcode</th>
                                            <th class="min-tablet">Nama Item</th>
                                            <th class="min-tablet">Stok</th>
                                            <th class="min-tablet">Satuan</th>
                                            <th class="min-tablet">Rak</th>
                                            <th class="min-tablet">Jenis</th>
                                            <th class="min-desktop">Harga Jual</th>
                                            <th class="min-desktop">Merek</th>
                                            <th class="min-desktop">Tipe</th>
                                            <th class="min-desktop">Keterangan</th>
                                            <th class="min-desktop">Supplier</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td>1</td>
                                              <td>0010100120</td>
                                              <td>as</td>
                                              <td>nama item</td>
                                              <td>1</td>
                                              <td>DUS</td>
                                              <td>rak 1</td>
                                              <td>jenis 1</td>
                                              <td>2000,00</td>
                                              <td>merek 1</td>
                                              <td>INV</td>
                                              <td>baru</td>
                                              <td>supplier 1</td>
                                          </tr>
                                      </tbody>
                                  </table>
                              {{-- </div> --}}
                            {{-- </div> --}}
                            <br>
                            <div class="form-group">
                                <div class="col-md-2 col-xs-2">
                                    <div class="btn btn-success btn-labeled fa fa-plus" id="tambah-item" data-toggle="modal" data-target="#tambahItem">Tambah Item</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <a href="#" id="lihat-harga" class="btn btn-default btn-labeled fa fa-money" data-toggle="modal" data-target="#lihatHarga">Lihat Harga</a>
                                </div>
                            </div>
                        </div>
                        {{-- End of modal body --}}
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="button" name="pilih" class="btn btn-primary">Pilih</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal fade" id="tambahItem" tabindex="-1" role="dialog" aria-labelledby="tambahItemTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Tambah Item</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        {{-- Start of modal body --}}
                        <div class="modal-body">
                            <div class="col-md-6">
                                <div class= "panel">
                                    <!--Block Styled Form -->
                                    <!--===================================================-->
                                    {{-- <form class="form-horizontal"> --}}
                                        <div class="panel-body">
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Tipe Item</label>
                                              <div class="col-xs-8">
                                                <div class="col-md-6 pad-no">
                                                    <!-- Icon Radio Buttons -->
                                                    <div class="radio">
                                                        <label class="form-radio form-icon">
                                                        <input type="radio" name="tipe-item" value="barang"> Barang (INV)</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="form-radio form-icon">
                                                        <input type="radio" name="tipe-item" value="rakitan"> Rakitan (ASM)</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="form-radio form-icon">
                                                        <input type="radio" name="tipe-item" value="non-inventory"> Non Inventory (NON-INV)</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label class="form-radio form-icon">
                                                        <input type="radio" name="tipe-item" value="jasa"> Jasa (SRV)</label>
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Kode Item*</label>
                                              <div class="col-xs-8">
                                                  <input type="text" class="form-control" name="kode-item" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Nama Item*</label>
                                              <div class="col-xs-8">
                                                  <input type="text" class="form-control" name="nama-item" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Jenis*</label>
                                              <div class="col-xs-6">
                                                <select class="form-control selectpicker" data-live-search="true">
                                                    <option value="makanan">Makanan</option>
                                                    <option value="minuman">Minuman</option>
                                                </select>
                                              </div>
                                              <div class="col-xs-3">
                                                  <div class="btn btn-default btn-icon icon-md fa fa-plus" data-toggle="modal" data-target="#tambahJenis"></div>
                                              </div>
                                              <div class="modal fade" id="tambahJenis" tabindex="-1" role="dialog" aria-labelledby="tambahJenisTitle" aria-hidden="true">
                                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Jenis Item</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-body">
                                                          <table class="table table-striped table-bordered">
                                                              <thead>
                                                                  <tr>
                                                                      <th>Jenis</th>
                                                                      <th>Keterangan</th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody>
                                                                  <tr>
                                                                      <td><input type="text" class="form-control" name="jenis" /></td>
                                                                      <td><input type="text" class="form-control" name="keterangan" /></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td><input type="text" class="form-control" name="jenis" /></td>
                                                                      <td><input type="text" class="form-control" name="keterangan" /></td>
                                                                  </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        <button type="button" class="btn btn-primary">Hapus</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Merek</label>
                                              <div class="col-xs-6">
                                                <select class="form-control selectpicker" data-live-search="true">
                                                    <option value="indofood">Indofood</option>
                                                </select>
                                              </div>
                                              <div class="col-xs-3">
                                                  <div class="btn btn-default btn-icon icon-md fa fa-plus" data-toggle="modal" data-target="#tambahMerek"></div>
                                              </div>
                                              <div class="modal fade" id="tambahMerek" tabindex="-1" role="dialog" aria-labelledby="tambahMerekTitle" aria-hidden="true">
                                                  <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Jenis Item</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                          <span aria-hidden="true">&times;</span>
                                                        </button>
                                                      </div>
                                                      <div class="modal-body">
                                                          <table class="table table-striped table-bordered">
                                                              <thead>
                                                                  <tr>
                                                                      <th>Merek</th>
                                                                      <th>Keterangan</th>
                                                                  </tr>
                                                              </thead>
                                                              <tbody>
                                                                  <tr>
                                                                      <td><input type="text" class="form-control" name="merek" /></td>
                                                                      <td><input type="text" class="form-control" name="keterangan" /></td>
                                                                  </tr>
                                                                  <tr>
                                                                      <td><input type="text" class="form-control" name="merek" /></td>
                                                                      <td><input type="text" class="form-control" name="keterangan" /></td>
                                                                  </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                      <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                        <button type="button" class="btn btn-primary">Hapus</button>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Mata Uang</label>
                                              <div class="col-xs-8">
                                                  <input type="text" class="form-control" name="mata-uang" value="IDR" readonly />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Rak</label>
                                              <div class="col-xs-8">
                                                  <input type="text" class="form-control" name="rak" />
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Dept. / Gudang</label>
                                              <div class="col-xs-8">
                                                <select class="form-control">
                                                    <option value="gudang">Gudang</option>
                                                    <option value="utama">Utama</option>
                                                </select>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Keterangan</label>
                                              <div class="col-xs-8">
                                                  <textarea name="keterangan" placeholder="Keterangan" class="form-control"></textarea>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Status Jual</label>
                                              <div class="col-xs-8">
                                                  <div class="radio">
                                                      <label class="form-radio form-icon">
                                                          <input type="radio" name="status-jual" value="masih-dijual"> Masih dijual
                                                      </label>
                                                      <label class="form-radio form-icon">
                                                          <input type="radio" name="status-jual" value="tidak-dijual"> Tidak dijual
                                                      </label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <label class="col-xs-3 control-label">Pilihan</label>
                                              <div class="col-xs-8">
                                                  <div class="checkbox">
                                                      <input type="checkbox" name="pilihan" value="serial"> Serial
                                                  </div>
                                                  <div class="checkbox">
                                                      <input type="checkbox" name="pilihan" value="tidak-dapat-poin"> Tidak dapat point
                                                  </div>
                                              </div>
                                          </div>
                                        </div>
                                    {{-- </form> --}}
                                    <!--===================================================-->
                                    <!--End Block Styled Form -->
                                </div>
                  
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Lain - lain</h3>
                                    </div>
                                    <!-- Form-->
                                    <!--===================================================-->
                                    {{-- <form class="form-horizontal"> --}}
                                        <div class="panel-body">
                                            <div class="form-group">
                                              <label class="col-xs-3 control-label">Stok Minimum</label>
                                              <div class="col-xs-8">
                                                <input type="text" class="form-control" name="stok-minimum" />
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="col-xs-3 control-label">Gambar</label>
                                              <div class="col-xs-8">
                                                <img style="border:1px">
                                                <input type="file" name="foto-stok-minimun" id="foto-stok-minimun">
                                              </div>
                                            </div>
                                        </div>
                                    {{-- </form> --}}
                                    <!--===================================================-->
                                    <!--End  Form-->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Harga Jual</h3>
                                    </div>
                                    <!--Horizontal Form-->
                                    <!--===================================================-->
                                    {{-- <form class="form-horizontal"> --}}
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Satuan*</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control selectpicker">
                                                        <option value="dus">DUS</option>
                                                        <option value="pak">PAK</option>
                                                        <option value="pcs">PCS</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Pilihan Harga Jual*</label>
                                                <div class="col-sm-8">
                                                  <select id="pilihanhargajual" class="form-control selectpicker">
                                                      <option value="harga">Satu Harga</option>
                                                      <option value="jumlah">Berdasarkan Jumlah</option>
                                                  </select>
                                                </div>
                                            </div>
                                            <div id="p1" class="form-group pilihanhargajual">
                                                <label class="col-xs-3 control-label">Harga jual*</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="harga-jual" />
                                                </div>
                                            </div>
                                        </div>
                                    {{-- </form> --}}
                                    <!--===================================================-->
                                    <!--End Horizontal Form-->
                                </div>
                  
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Potongan Harga</h3>
                                    </div>
                                    <!--Horizontal Form-->
                                    <!--===================================================-->
                                    {{-- <form class="form-horizontal"> --}}
                                        <div class="panel-body">
                                            <div class="form-group">
                                              <div class="col-xs-1">
                                              </div>
                                                <label class="col-xs-5 ">Group Pelanggan</label>
                                                <label class="col-xs-5 ">Potongan (%)</label>
                                            </div>
                                            <div class="form-group">
                                              <div class="col-xs-1">
                                              </div>
                                              <div class="col-xs-5">
                                                <select class="form-control selectpicker">
                                                    <option value="dus">DUS</option>
                                                    <option value="pak">PAK</option>
                                                    <option value="pcs">PCS</option>
                                                </select>
                                              </div>
                                                <div class="col-xs-5">
                                                    <input type="text" class="form-control"/>
                                                </div>
                                            </div>
                                        </div>
                                    {{-- </form> --}}
                                    <!--===================================================-->
                                    <!--End Horizontal Form-->
                                </div>
                            </div>
                        </div>
                        {{-- End of modal body --}}
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="button" name="simpan" class="btn btn-primary">Simpan</button>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal fade" id="lihatHarga" tabindex="-1" role="dialog" aria-labelledby="lihatHargaTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Lihat Harga</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        {{-- Start of modal body --}}
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-md-2 col-sm-2 control-label">Nama Item</label>
                                <div class="col-md-4 col-sm-4">
                                  <input type="text" class="form-control" name="nama-item" disabled />
                                </div>
                            </div>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>Keterangan</th>
                                        <th>Harga Jual</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" class="form-control" name="keterangan" style="border: 0px solid" value="Harga Satuan" disabled /></td>
                                        <td><input type="text" class="form-control" name="harga-jual" style="border: 0px solid" value="12000" disabled /></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        {{-- End of modal body --}}
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <input type="text" class="form-control" name="nama-item" disabled />
                  </div>
              </div>
              <div class="form-group pad-ver-5">
                <label class="col-md-2 col-xs-2">Periode</label>
                <div class="col-md-4 col-xs-4">
                    <input type="month" name="periode" min="2000-01" value="2000-01" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2">Dept. / Gudang</label>
                <div class="col-md-3">
                  <select class="form-control" data-live-search="true">
                      <option value="gudang">Gudang</option>
                      <option value="utm">Utama</option>
                      <option value="semua">Semua</option>
                  </select>
                </div>
                <div class="col-md-1">
                    <div class="btn btn-default btn-labeled fa fa-gear" id="proses">Proses</div>
                </div>
            </div>
            <table id="demo-dt-delete" class="table table-striped table-bordered" width="1800px">
                <thead>
                    <tr>
                      <th>No Transaksi</th>
                      <th>Kantor</th>
                      <th class="min-tablet">Tanggal</th>
                      <th class="min-tablet">Tipe</th>
                      <th class="min-tablet">Keterangan</th>
                      <th class="min-desktop">Masuk</th>
                      <th class="min-desktop">Keluar</th>
                      <th class="min-desktop">Saldo</th>
                      <th class="min-desktop">Supplier/Pelanggan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Albert Desouzaasdfsadfdsa</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td>Albert Desouza</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <label class="col-md-1 col-xs-5">Total Masuk</label>
                <div class="col-md-2 col-xs-7">
                    <input type="text" class="form-control" name="total-masuk"/>
                </div>
                <label class="col-md-1 col-xs-5">Saldo Awal</label>
                <div class="col-md-2 col-xs-7">
                    <input type="text" class="form-control" name="saldo-awal"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-1 col-xs-5">Total Keluar</label>
                <div class="col-md-2 col-xs-7">
                    <input type="text" class="form-control" name="total-keluar"/>
                </div>
                <label class="col-md-1 col-xs-5">Saldo Akhir</label>
                <div class="col-md-2 col-xs-7">
                    <input type="text" class="form-control" name="saldo-akhir"/>
                </div>
            </div>
        </div>
      </form>
    </div>
    <!--===================================================-->
    <!-- End     selection (single row) -->
</div>
@endsection
@section('script')
<!--jQuery [ REQUIRED ]-->
<script src="{{ asset('atem/js/jquery-2.1.1.min.js') }}"></script>
<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/bootstrap.min.js') }}"></script>
<!--Fast Click [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/fast-click/fastclick.min.js') }}"></script>
<!--Jquery Nano Scroller js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/nanoscrollerjs/jquery.nanoscroller.min.js') }}"></script>
<!--Metismenu js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/metismenu/metismenu.min.js') }}"></script>
<!--Jasmine Admin [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/scripts.js') }}"></script>
<!--Switchery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/switchery/switchery.min.js') }}"></script>
<!--Bootstrap Select [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<!--DataTables [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<!--Fullscreen jQuery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/screenfull/screenfull.js') }}"></script>
<!--DataTables Sample [ SAMPLE ]-->
<script>
  // Row selection and deletion (multiple rows)
  // -----------------------------------------------------------------
  var rowDeletion = $('#demo-dt-delete').DataTable({
    //"responsive": true,
    "scrollX": true,
    "searching": false,
    "paging": false,
    "info": false,
    "dom": 'frtip<"toolbar">',
    "aoColumnDefs": [
        { "bSortable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11] },
        { "bSearchable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11 ] }
    ]
  });

  $('#demo-custom-toolbar').appendTo($("div.toolbar"));

  $('#demo-dt-delete tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
      $(this).removeClass('selected');
    }
    else {
      rowDeletion.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      //$(this).toggleClass('selected');
    }
  } );

  $('#hapusitem').click( function () {
    rowDeletion.row('.selected').remove().draw( false );
  } );

//   $('#openBtn').click(function() {
//       $('#myModal').modal({
//           show: true
//         });
//     })
</script>
@endsection

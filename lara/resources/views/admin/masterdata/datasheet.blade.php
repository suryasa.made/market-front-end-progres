@extends('admin/template/template')
@section('maintitle', 'Data Item')
@section('title', 'Datasheet')
@section('main')
<div id="page-content">

    <!-- Row selection (single row) -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Datasheet</h3>
        </div>
        <form class="form-horizontal">
            <div class="panel-body">
              <div class="form-group">
                <label class="col-md-2 col-sm-2">Kata Kunci 1</label>
                <div class="col-md-4 col-sm-4">
                  <input type="text" class="form-control" name="kode-item" />
                </div>
                <label class="col-md-1 col-sm-1">Merek</label>
                <div class="col-md-2 col-sm-2">
                    <select class="form-control" name="merek">
                        <option value="merek1">Merek 1</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 col-sm-2">Kata Kunci 2</label>
                <div class="col-md-4 col-sm-4">
                  <input type="text" class="form-control" name="kata-kunci2" />
                </div>
                <label class="col-md-3 col-sm-3"> Jenis / Satuan / Rak</label>
            </div>
            <div class="form-group pad-ver-5">
                <label class="col-xs-2">Tipe Harga Jual</label>
                <div class="col-md-5 col-xs-5">
                  {{-- <div class="radio"> --}}
                      <!-- Icon Radio Buttons -->
                      <div class="radio">
                        <label class="form-radio form-icon">
                        <input type="radio" name="tipe-harga" value="tanpa-harga"> Tanpa Harga</label>
                        <label class="form-radio form-icon">
                        <input type="radio" name="tipe-harga" value="satu-harga"> Satu Harga</label>
                        <label class="form-radio form-icon">
                        <input type="radio" name="tipe-harga" value="satuan"> Satuan</label>
                        <label class="form-radio form-icon">
                        <input type="radio" name="tipe-harga" value="jumlah"> Jumlah</label>
                        <label class="form-radio form-icon">
                        <input type="radio" name="tipe-harga" value="level"> Level</label>
                    </div>
                  {{-- </div> --}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-2 col-sm-2">Urut berdasar</label>
                <div class="col-md-3 col-sm-3">
                  <select class="form-control">
                      <option value="kode-item">Kode Item</option>
                      <option value="nama-item">Nama Item</option>
                      <option value="jenis">Jenis</option>
                      <option value="merek">Merek</option>
                      <option value="keterangan">Keterangan</option>
                      <option value="supplier">Supplier</option>
                  </select>
                </div>
                <div class="col-md-2 col-sm-2">
                  <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-asc" id="sort-asc"></div>
                  <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-desc" id="sort-desc"></div>
                </div>
                <div class="col-md-1 col-sm-1">
                </div>
                <div class="col-md-2 col-sm-2">
                    <div class="btn btn-default btn-icon icon-lg fa fa-search" id="search"></div>
                    <div class="btn btn-default btn-icon icon-lg fa fa-remove" id="reset"></div>
                </div>
            </div>
            <table id="demo-dt-delete" class="table table-striped table-bordered" width="1800px">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Item</th>
                      <th class="min-tablet">Barcode</th>
                      <th class="min-tablet">Nama Item</th>
                      <th class="min-desktop">Jenis</th>
                      <th class="min-desktop">Merek</th>
                      <th class="min-desktop">Rak</th>
                      <th class="min-desktop">Tipe Item</th>
                      <th class="min-desktop">Harga Pokok</th>
                      <th class="min-desktop">Harga Jual</th>
                      <th class="min-desktop">Satuan</th>
                      <th class="min-desktop">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>item1</td>
                        <td>01010112</td>
                        <td>item 1</td>
                        <td>jenis 1</td>
                        <td>merek 1</td>
                        <td>rak 1</td>
                        <td>tipe 1</td>
                        <td>1000,00</td>
                        <td>2000,00</td>
                        <td>DUS</td>
                        <td>Baru</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <div class="form-group">
                <div class="col-md-2 col-xs-2">
                    <div class="btn btn-default btn-labeled fa fa-edit" id="edit-harga-jual" data-toggle="modal" data-target="#editHargaJual">Edit Harga Jual</div>
                </div>
                <div class="modal fade" id="editHargaJual" tabindex="-1" role="dialog" aria-labelledby="editHargaJualTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Harga Jual</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                          <table class="table table-striped table-bordered">
                              <thead>
                                  <tr>
                                      <th>Kode Item</th>
                                      <th>Harga Jual</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td>item1</td>
                                      <td><input type="text" class="form-control" name="harga-jual" value="2000,00" /></td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="button" name="simpan-harga" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
      </form>
    </div>
    <!--===================================================-->
    <!-- End     selection (single row) -->
</div>
@endsection
@section('script')
<!--jQuery [ REQUIRED ]-->
<script src="{{ asset('atem/js/jquery-2.1.1.min.js') }}"></script>
<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/bootstrap.min.js') }}"></script>
<!--Fast Click [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/fast-click/fastclick.min.js') }}"></script>
<!--Jquery Nano Scroller js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/nanoscrollerjs/jquery.nanoscroller.min.js') }}"></script>
<!--Metismenu js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/metismenu/metismenu.min.js') }}"></script>
<!--Jasmine Admin [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/scripts.js') }}"></script>
<!--Switchery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/switchery/switchery.min.js') }}"></script>
<!--Bootstrap Select [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<!--DataTables [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<!--Fullscreen jQuery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/screenfull/screenfull.js') }}"></script>
<!--DataTables Sample [ SAMPLE ]-->
<script>
  // Row selection and deletion (multiple rows)
  // -----------------------------------------------------------------
  var rowDeletion = $('#demo-dt-delete').DataTable({
    //"responsive": true,
    "scrollX": true,
    "searching": false,
    "paging": false,
    "info": false,
    "dom": 'frtip<"toolbar">',
    "aoColumnDefs": [
        { "bSortable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10 ] },
        { "bSearchable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10 ] }
    ]
  });

  $('#demo-custom-toolbar').appendTo($("div.toolbar"));

  $('#demo-dt-delete tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
      $(this).removeClass('selected');
    }
    else {
      rowDeletion.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      //$(this).toggleClass('selected');
    }
  } );

  $('#hapusitem').click( function () {
    rowDeletion.row('.selected').remove().draw( false );
  } );
</script>
@endsection

@extends('admin/template/template')
@section('maintitle', 'Data Item')
@section('title', 'Diskon Periode')
@section('main')
<div id="page-content">

    <!-- Row selection (single row) -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Diskon Periode</h3>
        </div>
        <form class="form-horizontal">
            <div class="panel-body">
            <table class="table table-striped table-bordered" width="1800px">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Potongan</th>
                      <th class="min-tablet">Tipe Periode</th>
                      <th class="min-tablet">Aktif</th>
                      <th class="min-tablet">Prioritas</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>agus20</td>
                        <td>Potongan Berdasarkan Tanggal dan Tanggal</td>
                        <td style="text-align: center">
                            <input type="checkbox" name="status" value="aktif" checked disabled>
                        </td>
                        <td style="text-align: right">1</td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <div class="col-md-1 col-xs-2">
                    <div class="btn btn-success btn-labeled fa fa-plus" id="tambah-edit-diskon-periode" data-toggle="modal" data-target="#tambahEditDiskonPeriode">Tambah</div>
                </div>
                <div class="modal fade" id="tambahEditDiskonPeriode" tabindex="-1" role="dialog" aria-labelledby="tambahEditDiskonPeriodeTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Tambah / Edit Diskon Periode</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        {{-- Start of modal body --}}
                        <div class="modal-body">
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Kode Potongan</label>
                                      <div class="col-md-3 col-sm-3">
                                        <input type="text" class="form-control" name="kode-potongan" />
                                      </div>
                                      <div class="col-md-1 col-sm-1"></div>
                                      <div class="col-md-2 col-sm-2">
                                        <label class="form-checkbox form-icon control-label">
                                            <input type="checkbox" name="status" value="status"> Potongan Aktif</label>
                                      </div>
                                      <label class="col-md-2 col-sm-2 control-label">Prioritas</label>
                                      <div class="col-md-2 col-sm-2">
                                        <input type="number" class="form-control" name="prioritas" placeholder="0" />
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Sistem Potongan</label>
                                      <div class="col-md-7 col-sm-7">
                                        <div class="radio">
                                            <label class="form-radio form-icon">
                                                <input type="radio" name="sistem-potongan" value="tanggal-jam"> Tanggal & Jam
                                            </label>
                                            <label class="form-radio form-icon">
                                                <input type="radio" name="sistem-potongan" value="tanggal-jam-berulang"> Tanggal, Jam Berulang
                                            </label>
                                            <label class="form-radio form-icon">
                                                <input type="radio" name="sistem-potongan" value="berulang"> Berulang Tiap Hari
                                            </label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Tgl & Jam Dari</label>
                                      <div class="col-md-3 col-sm-3">
                                        <input type="date" class="form-control" name="tgl-jam-dari" />
                                      </div>
                                      <label class="col-md-1 col-sm-1">s/d</label>
                                      <div class="col-md-3 col-sm-3">
                                        <input type="date" class="form-control" name="tgl-jam-sampai" />
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 control-label">Dari Jam</label>
                                        <div class="col-md-3 col-sm-3">
                                          <input type="time" class="form-control" name="dari-jam" />
                                        </div>
                                        <label class="col-md-1 col-sm-1">s/d</label>
                                        <div class="col-md-3 col-sm-3">
                                          <input type="time" class="form-control" name="dari-jam" />
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="btn btn-default btn-labeled fa fa-search" id="pilih-item" data-toggle="modal" data-target="#pilihItem"> Pilih Item</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Pilihan Hari</label>
                                      <div class="col-md-10 col-sm-10">
                                        <div class="checkbox">
                                            <label class="form-checkbox form-icon">
                                            <input type="checkbox" name="pilihan" value="senin"> Senin </label>
                                            <label class="form-checkbox form-icon">
                                            <input type="checkbox" name="pilihan" value="selasa"> Selasa </label>
                                            <label class="form-checkbox form-icon">
                                            <input type="checkbox" name="pilihan" value="rabu"> Rabu </label>
                                            <label class="form-checkbox form-icon">
                                            <input type="checkbox" name="pilihan" value="kamis"> Kamis </label>
                                            <label class="form-checkbox form-icon">
                                            <input type="checkbox" name="pilihan" value="jumat"> Jumat </label>
                                            <label class="form-checkbox form-icon">
                                            <input type="checkbox" name="pilihan" value="sabtu"> Sabtu </label>
                                            <label class="form-checkbox form-icon">
                                            <input type="checkbox" name="pilihan" value="minggu"> Minggu </label>
                                        </div>
                                      </div>
                                    </div>
                                  <table class="table table-striped table-bordered" width="1800px">
                                      <thead>
                                          <tr>
                                            <th>No</th>
                                            <th>Kode Item</th>
                                            <th class="min-tablet">Nama Item</th>
                                            <th class="min-tablet">Satuan</th>
                                            <th class="min-desktop">Pot. 1</th>
                                            <th class="min-desktop">Pot. 2</th>
                                            <th class="min-desktop">Pot. 3</th>
                                            <th class="min-desktop">Pot. 4</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                          <tr>
                                              <td>1</td>
                                              <td>item1</td>
                                              <td>nama item</td>
                                              <td>Dus</td>
                                              <td>12,00</td>
                                              <td>0,00</td>
                                              <td>0,00</td>
                                              <td>0,00</td>
                                          </tr>
                                      </tbody>
                                  </table>
                            <div class="form-group">
                                <div class="col-md-2 col-xs-2">
                                    <div class="btn btn-pink btn-labeled fa fa-minus" id="hapus-baris" data-toggle="modal" data-target="#hapusBaris">Hapus Baris</div>
                                </div>
                            </div>
                        </div>
                        {{-- End of modal body --}}
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="button" name="simpan" class="btn btn-primary">Simpan</button>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="modal fade" id="pilihItem" tabindex="-1" role="dialog" aria-labelledby="pilihItemTitle" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title">Daftar Item</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        {{-- Start of modal body --}}
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 control-label">Kata Kunci 1</label>
                                    <div class="col-md-4 col-sm-4">
                                      <input type="text" class="form-control" name="kata-kunci1" />
                                    </div>
                                    <label class="col-md-2 col-sm-2 control-label">Merek</label>
                                    <div class="col-md-4 col-sm-4">
                                      <select class="form-control" name="merek">
                                          <option value="merek1">Merek 1</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 col-sm-2 control-label">Kata Kunci 2</label>
                                    <div class="col-md-4 col-sm-4">
                                      <input type="text" class="form-control" name="kata-kunci2" />
                                    </div>
                                    <label class="col-md-3 col-sm-3"> Jenis / Satuan / Rak</label>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-2 col-sm-2 control-label">Kata Kunci 3</label>
                                    <div class="col-md-4 col-sm-4">
                                      <input type="text" class="form-control" name="kata-kunci3" />
                                    </div>
                                    <label class="col-md-2 col-sm-2"> Keterangan</label>
                                    <label class="col-md-2 col-sm-2 control-label">Kantor / Gudang</label>
                                    <div class="col-md-2 col-sm-2">
                                      <select class="form-control" name="lokasi">
                                          <option value="gudang">Gudang</option>
                                          <option value="utama">Utama</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                      <label class="col-md-2 col-sm-2 control-label">Kode Supplier</label>
                                      <div class="col-md-4 col-sm-4">
                                        <input type="text" class="form-control" name="kode-supplier" />
                                      </div>
                                    </div>
                                  <div class="form-group">
                                    <label class="col-md-2 col-sm-2 control-label">Urut berdasar</label>
                                    <div class="col-md-3 col-sm-3">
                                      <select class="form-control">
                                          <option value="kode-item">Kode Item</option>
                                          <option value="nama-item">Nama Item</option>
                                          <option value="jenis">Jenis</option>
                                          <option value="merek">Merek</option>
                                          <option value="keterangan">Keterangan</option>
                                          <option value="supplier">Supplier</option>
                                      </select>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-asc" id="sort-asc"></div>
                                        <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-desc" id="sort-desc"></div>
                                    </div>
                                    <div class="col-md-1 col-sm-1"></div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="btn btn-default btn-icon icon-lg fa fa-search" id="search"></div>
                                        <div class="btn btn-default btn-icon icon-lg fa fa-remove" id="reset"></div>
                                    </div>
                                  </div>
                                <table id="demo-dt-delete" class="table table-striped table-bordered" width="1800px">
                                    <thead>
                                        <tr>
                                          <th>No</th>
                                          <th>Kode Item</th>
                                          <th>Barcode</th>
                                          <th class="min-tablet">Nama Item</th>
                                          <th class="min-tablet">Stok</th>
                                          <th class="min-tablet">Satuan</th>
                                          <th class="min-tablet">Rak</th>
                                          <th class="min-tablet">Jenis</th>
                                          <th class="min-desktop">Harga Jual</th>
                                          <th class="min-desktop">Merek</th>
                                          <th class="min-desktop">Tipe</th>
                                          <th class="min-desktop">Keterangan</th>
                                          <th class="min-desktop">Supplier</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>0010100120</td>
                                            <td>as</td>
                                            <td>nama item</td>
                                            <td>1</td>
                                            <td>DUS</td>
                                            <td>rak 1</td>
                                            <td>jenis 1</td>
                                            <td>2000,00</td>
                                            <td>merek 1</td>
                                            <td>INV</td>
                                            <td>baru</td>
                                            <td>supplier 1</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        {{-- End of modal body --}}
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="button" name="simpan" class="btn btn-primary">Simpan</button>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-md-1 col-xs-2">
                    <div class="btn btn-default btn-labeled fa fa-edit" id="tambah-edit-diskon-periode" data-toggle="modal" data-target="#tambahEditDiskonPeriode">Edit</div>
                </div>
                <div class="col-md-1 col-xs-2">
                    <div class="btn btn-pink btn-labeled fa fa-minus" id="hapus-diskon-periode">Hapus</div>
                </div>
            </div>
        </div>
      </form>
    </div>
    <!--===================================================-->
    <!-- End     selection (single row) -->
</div>
@endsection
@section('script')
<!--jQuery [ REQUIRED ]-->
<script src="{{ asset('atem/js/jquery-2.1.1.min.js') }}"></script>
<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/bootstrap.min.js') }}"></script>
<!--Fast Click [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/fast-click/fastclick.min.js') }}"></script>
<!--Jquery Nano Scroller js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/nanoscrollerjs/jquery.nanoscroller.min.js') }}"></script>
<!--Metismenu js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/metismenu/metismenu.min.js') }}"></script>
<!--Jasmine Admin [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/scripts.js') }}"></script>
<!--Switchery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/switchery/switchery.min.js') }}"></script>
<!--Bootstrap Select [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<!--DataTables [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<!--Fullscreen jQuery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/screenfull/screenfull.js') }}"></script>
<!--DataTables Sample [ SAMPLE ]-->
<script>
    // Row selection and deletion (multiple rows)
    // -----------------------------------------------------------------
    var rowDeletion = $('#demo-dt-delete').DataTable({
      //"responsive": true,
      "scrollX": true,
      "searching": false,
      "paging": false,
      "info": false,
      "dom": 'frtip<"toolbar">',
      "aoColumnDefs": [
          { "bSortable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11] },
          { "bSearchable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11] }
      ]
    });
  
    $('#demo-custom-toolbar').appendTo($("div.toolbar"));
  
    $('#demo-dt-delete tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
      }
      else {
        rowDeletion.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        $(this).toggleClass('selected');
      }
    } );
  
    $('#hapusitem').click( function () {
      rowDeletion.row('.selected').remove().draw( false );
    } );
  
  //   $('#openBtn').click(function() {
  //       $('#myModal').modal({
  //           show: true
  //         });
  //     })
  </script>
@endsection

@extends('admin/template/template')
@section('maintitle', 'Data Item')
@section('title', 'Daftar Item')
@section('main')
<div id="page-content">

    <!-- Row selection (single row) -->
    <!--===================================================-->
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Daftar Item</h3>
        </div>
        <div id="demo-custom-toolbar" class="table-toolbar-left" style="margin:15px 0;">
            {{-- <a href="tambah_item" data-target="#tambah-item" data-toggle="modal" id="tambahitem" class="btn btn-success">Tambah Item</a> --}}
            <a href="tambah_item" class="btn btn-success">Tambah Item</a>
            <a href="edit_item" id="edititem" class="btn btn-warning" style="margin:0 5px;">Edit Item</a>
            <button id="hapusitem" class="btn btn-pink">Hapus Item</button>
        </div>
        <form class="form-horizontal">
          <div class="panel-body">
              <div class="form-group">
                <label class="col-md-2 col-sm-2">Kata Kunci</label>
                <div class="col-md-8 col-sm-8">
                  <input type="text" class="form-control" name="kata-kunci" />
                </div>
                <div class="col-md-2 col-sm-2">
                  <div class="btn btn-default btn-icon icon-lg fa fa-search" id="search"></div>
                  <div class="btn btn-default btn-icon icon-lg fa fa-remove" id="reset"></div>
                </div>
              </div>
              <div class="form-group pad-ver-5">
                <label class="col-md-2 col-xs-2">Tipe Item</label>
                <div class="col-md-10 col-xs-10">
                  <div class="radio">
                    <label class="form-radio form-icon">
                    <input type="radio" name="tipe-item" value="barang"> Barang</label>
                    <label class="form-radio form-icon">
                    <input type="radio" name="tipe-item" value="rakitan"> Rakitan</label>
                    <label class="form-radio form-icon active">
                    <input type="radio" name="tipe-item" value="non-inventory"> Non Inventory</label>
                    <label class="form-radio form-icon active">
                    <input type="radio" name="tipe-item" value="jasa"> Jasa</label>
                    <label class="form-radio form-icon active">
                    <input type="radio" name="tipe-item" value="semua"> Semua</label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2">Dept. / Gudang</label>
                <div class="col-md-3">
                  <select class="form-control" data-live-search="true">
                      <option value="gudang">Gudang</option>
                      <option value="utm">Utama</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-2 col-sm-2">Urut berdasar</label>
                <div class="col-md-3 col-sm-3">
                  <select class="form-control">
                      <option value="kode-item">Kode Item</option>
                      <option value="nama-item">Nama Item</option>
                      <option value="jenis">Jenis</option>
                      <option value="merek">Merek</option>
                      <option value="keterangan">Keterangan</option>
                      <option value="supplier">Supplier</option>
                  </select>
                </div>
                <div class="col-md-2 col-sm-2">
                  <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-asc" id="sort-asc"></div>
                  <div class="btn btn-default btn-icon icon-lg fa fa-sort-amount-desc" id="sort-desc"></div>
                </div>
              </div>
            <table id="demo-dt-delete" class="table table-striped table-bordered" width="1800px">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode Item</th>
                      <th class="min-tablet">Nama Item</th>
                      <th class="min-tablet">Stok</th>
                      <th class="min-tablet">Satuan</th>
                      <th class="min-desktop">Jenis</th>
                      <th class="min-desktop">Merek</th>
                      <th class="min-desktop">Harga Pokok</th>
                      <th class="min-desktop">Harga Jual</th>
                      <th class="min-desktop">Tipe</th>
                      <th class="min-desktop">Mata Uang</th>
                      <th class="min-desktop">Stok Min</th>
                      <th class="min-desktop">Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Albert Desouzaasdfsadfdsa</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td>Albert Desouza</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>$320,800</td>
                        <td>$320,800</td>
                    </tr>
                </tbody>
            </table>
        </div>
      </form>
    </div>
    <!--===================================================-->
    <!-- End     selection (single row) -->
</div>
@endsection
@section('script')
<!--jQuery [ REQUIRED ]-->
<script src="{{ asset('atem/js/jquery-2.1.1.min.js') }}"></script>
<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/bootstrap.min.js') }}"></script>
<!--Fast Click [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/fast-click/fastclick.min.js') }}"></script>
<!--Jquery Nano Scroller js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/nanoscrollerjs/jquery.nanoscroller.min.js') }}"></script>
<!--Metismenu js [ REQUIRED ]-->
<script src="{{ asset('atem/plugins/metismenu/metismenu.min.js') }}"></script>
<!--Jasmine Admin [ RECOMMENDED ]-->
<script src="{{ asset('atem/js/scripts.js') }}"></script>
<!--Switchery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/switchery/switchery.min.js') }}"></script>
<!--Bootstrap Select [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<!--DataTables [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('atem/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<!--Fullscreen jQuery [ OPTIONAL ]-->
<script src="{{ asset('atem/plugins/screenfull/screenfull.js') }}"></script>
<!--DataTables Sample [ SAMPLE ]-->
<script>
  // Row selection and deletion (multiple rows)
  // -----------------------------------------------------------------
  var rowDeletion = $('#demo-dt-delete').DataTable({
    //"responsive": true,
    "scrollX": true,
    "searching": false,
    "paging": false,
    "info": false,
    "dom": 'frtip<"toolbar">',
    "aoColumnDefs": [
        { "bSortable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12 ] },
        { "bSearchable": false, "aTargets": [ 0, 1, 2, 3,4,5,6,7,8,9,10,11,12 ] }
    ]
  });

  $('#demo-custom-toolbar').appendTo($("div.toolbar "));

  $('#demo-dt-delete tbody').on( 'click', 'tr', function () {
    if ( $(this).hasClass('selected') ) {
      $(this).removeClass('selected');
    }
    else {
      rowDeletion.$('tr.selected').removeClass('selected');
      $(this).addClass('selected');
      //$(this).toggleClass('selected');
    }
  } );

  $('#hapusitem').click( function () {
    rowDeletion.row('.selected').remove().draw( false );
  } );
</script>
@endsection

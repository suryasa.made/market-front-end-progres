<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminPageController extends Controller
{
    //
    public function index()
    {
        return view('homepage');
    }

    public function about()
    {
        return view('about');
    }

    public function itemdaftar()
    {
        return view('admin/masterdata/itemdaftar');
    }

    public function itemtambah()
    {
        return view('admin/masterdata/itemtambah');
    }

    public function itemedit()
    {
        return view('admin/masterdata/itemedit');
    }

    public function kartustok()
    {
        return view('admin/masterdata/kartustok');
    }

    public function datasheet()
    {
        return view('admin/masterdata/datasheet');
    }

    public function diskonperiode()
    {
        return view('admin/masterdata/diskonperiode');
    }
}

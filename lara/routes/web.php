<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/daftar_item', 'AdminPageController@itemdaftar');
Route::get('/tambah_item', 'AdminPageController@itemtambah');
Route::get('/edit_item', 'AdminPageController@itemedit');
Route::get('/kartu_stok', 'AdminPageController@kartustok');
Route::get('/datasheet', 'AdminPageController@datasheet');
Route::get('/diskon_periode', 'AdminPageController@diskonperiode');
